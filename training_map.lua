--dofile(lfs.writedir()..[[Scripts\training_map\training_map.lua]])

local module_folder = function(scriptfolder) return lfs.writedir() .. "Scripts\\" .. scriptfolder .. "\\?.lua;" end
package.path = module_folder("training_map") .. module_folder("training_map/groupbuilder").. package.path

local dcs = require "dcs"
local spawning = require "spawning"
local GroupBuilder = require "groupbuilder.groupbuilder"
local ctldLoader = require "ctld"
local nukeKicker = require "slmod_kick_for_nukes"
local rangeScript = require "rangescript"

local logger = dcs.Logger("Training_Map")

local dcsGroup = function(group_name)
    local groupData = mist.getGroupData(group_name, true)
    return GroupBuilder.New(group_name):withGroupData(groupData)
end

local rangeGroup = function(group_name) return dcsGroup(group_name):withCountry("CJTF-RED") end
local staticRanges = {
    "Easy1", "Easy1-3", "Easy1-4", "Easy2", "Easy3", "Easy Convoy",
    "Medium Convoy",
    "Hard1", "Hard2",
    "Extreme1","Extreme2","Extreme3",
    "Naval-2", "Naval-3",
    --Air drones.
    "AA Drones Easy 1", "AA Drones Easy 1-1","AA Drones Easy 1-2","AA Drones Easy 1-3","AA Drones Easy 1-4","AA Drones Easy 1-5","AA Drones Easy 1-6","AA Drones Easy 1-7",
    "AA Drones Medium 1","AA Drones Medium 1-1","AA Drones Medium 1-2","AA Drones Medium 1-3","AA Drones Medium 1-4","AA Drones Medium 1-5","AA Drones Medium 1-6","AA Drones Medium 1-7","AA Drones Medium 1-8",
}
for _, v in pairs(staticRanges) do
    logger.Log("Configuring Range [ " .. v .. " ].")
    spawning.KeepAlive(rangeGroup(v))
    logger.Log("Spawned and configured Range [ " .. v .. " ].")
end

local friendlyGroup = function(group_name) return dcsGroup(group_name):withCountry("USA") end
local friendlyGroups = { "Shell 4-1", "Shell 9-1", "Arco 7-1" , "Texaco 8-1", "Texaco 6-1", "Overlord 1-1"}

for _, v in pairs(friendlyGroups) do
    logger.Log("Configuring friendlyGroup [ " .. v .. " ].")
    spawning.KeepAlive(friendlyGroup(v))
    logger.Log("Launched friendlyGroup [ " .. v .. " ].")
end
